# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* Better documentation
* Unit tests
* Nicer, more consistent function names
* Example usage of spectrum alignment and mode clustering
* Make analysis work with `Unitful` quantities

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to minimize the amount of code and function repetition when implementing the above features
