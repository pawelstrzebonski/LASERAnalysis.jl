# spectrum.jl

Various functions for analyzing the optical spectrum of a laser.

```@autodocs
Modules = [LASERAnalysis]
Pages   = ["spectrum.jl"]
```
