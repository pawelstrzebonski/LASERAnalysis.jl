# References

LIV analysis is fairly standard laser analysis. The second derivative
of power is used to find threshold, and this is calculated as the
central finite difference on a non-uniform grid.

The spectral analysis makes use of phase correlation
[(wiki)](https://en.wikipedia.org/wiki/Phase_correlation)
to align spectra to each-other as they shift. Spectral mode
clustering is based on the
[Clustering](https://github.com/JuliaStats/Clustering.jl/)
package to identify spectral modes in a series of spectral measurements
(presumably taken as a function of driving current).
