# LIV Analysis

The "LIV" (power-current-voltage) measurement and analysis is perhaps the most basic and fundamental
laser measurement. This package provides a few utilities for analyzing
LIV measurement. To demonstrate, we'll start with idealized calculated
data. We define some of the fundamental parameters and calculate the
resulting LIV data. In order to keep track of units throughout our analysis,
we use the `Unitful` package to annotate the units of each quantity:

```@example fake
using Unitful

# Series resistance
r_s=50u"Ω"
# Diode voltage
v_d=1.4u"V"
# Slope (aka differential quantum) efficiency
eta=0.5
# Threshold current
i_th=3u"mA"

# Function to calculate the voltage and power values for our simulated diode laser
function livfun(i, i_th, v_d, r_s, eta)
    v = @. v_d + i * r_s
    l = @. (i - i_th > 0u"A") * (i - i_th) * v_d * eta
    uconvert.(u"mW", l), uconvert.(u"V", v)
end

# Current range
i=collect(0:0.5:10).*1u"mA"
# Calculate the voltage and output power
l, v=livfun(i, i_th, v_d, r_s, eta)
nothing # hide
```

We plot the (fake) LIV measurements using the `Plots` package:

```@example fake
import Plots: plot, plot!, twinx
import Plots: savefig # hide
plot(ustrip.(u"mA", i), ustrip.(u"V", v),
	xlabel="Current [mA]",
	ylabel="Voltage [V]",
	legend=false,
)
plot!(twinx(), ustrip.(u"mA", i), ustrip.(u"mW", l),
	xlabel="Current [mA]",
	ylabel="Power [mW]",
	legend=false,
	linecolor=:red,
)
savefig("fake_liv.png"); nothing # hide
```

![Simulated LIV measurements](fake_liv.png)

Now that we have our (simulated) LIV measurements, we can try to see what
device parameters `LASERAnalysis` can give us. First, we'll use the
`livthreshold` function to determine the threshold current, voltage, as well
as the threshold's index in our measurement array:

```@example fake
import LASERAnalysis

LASERAnalysis.livthreshold(l, i, v)
```

The function tells us that the threshold is at 3 \[mA\] and 1.55 \[V\]
(and that the threshold point is the 7th point in the data arrays). Now,
for more information than just the threshold, we use the `livstats`
function. However, this function does not work with `Unitful` (yet?),
so we'll strip off the units (but do so converting to units in terms
of \[W, A, V\] for simplicity) before calling it:

```@example fake
LASERAnalysis.livstats(ustrip.(u"W", l), ustrip.(u"A", i), ustrip.(u"V", v))
```

The function returns the threshold current, threshold voltage, series resistance,
and diode voltage. We can see that these match our theoretical values
(after converting units).

Now what about real data? Let's consider the following LIV data (taken
from a relatively small VCSEL). We calculate the threshold point and plot
it on the LIV plot:

```@example real
import Plots: plot, plot!, scatter!, twinx
import LASERAnalysis
import Plots: savefig # hide

# We import and assign the LIV data to (l, i, v) 
include("liv_data.jl") # hide

# We determine the threshold point
i_th, v_th, idx_th=LASERAnalysis.livthreshold(l, i, v)
l_th=l[idx_th]

plot(i, v,
	xlabel="Current [mA]",
	ylabel="Voltage [V]",
	legend=false,
)
plot!(twinx(), i, l,
	xlabel="Current [mA]",
	ylabel="Power [mW]",
	legend=false,
	linecolor=:red,
)
# We plot the threshold point on the LIV plot
scatter!([i_th], [l_th])
savefig("real_liv.png"); nothing # hide
```

![LIV measurements](real_liv.png)

Well, that's obviously wrong. So what happened and what can we do? The
threshold is found as the point of maximal curvature in the L-I curve
(maximal ``\frac{\partial^2 l}{\partial i^2}``), so some noise may lead to a relatively
large spike in the power that breaks our method. We can try to avoid that
by smoothing our measurement data using Gaussian filtering:

```@example real
# We Gaussian filter the power and voltage measurements
l_filtered=LASERAnalysis.gaussianfilter(l, 1, 10)
v_filtered=LASERAnalysis.gaussianfilter(v, 1, 10)

# We determine the threshold point
i_th, v_th, idx_th=LASERAnalysis.livthreshold(l_filtered, i, v_filtered)
l_th=l_filtered[idx_th]

plot(i, v_filtered,
	xlabel="Current [mA]",
	ylabel="Voltage [V]",
	legend=false,
)
plot!(twinx(), i, l_filtered,
	xlabel="Current [mA]",
	ylabel="Power [mW]",
	legend=false,
	linecolor=:red,
)
# We plot the threshold point on the LIV plot
scatter!([i_th], [l_th])
savefig("filtered_liv.png"); nothing # hide
```

![LIV measurements after smoothing](filtered_liv.png)

Now that looks better! Finally we calculate for the other laser parameters:

```@example real
LASERAnalysis.livstats(l_filtered, i, v_filtered)
```
