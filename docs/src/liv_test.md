# liv.jl

Basic unit tests for LIV analysis functions. We generate some fake (noise-free)
LIV measurements and put them through these functions, and check if the
results match the expected parameters we used to create the LIV data.
