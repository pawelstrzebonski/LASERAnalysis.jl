# filtering.md

Some basic filtering functions (mainly Gaussian smoothing filter) that
may be useful in pre-processing measurement data before putting it through
the various measurement analysis functions.

```@autodocs
Modules = [LASERAnalysis]
Pages   = ["filtering.jl"]
```
