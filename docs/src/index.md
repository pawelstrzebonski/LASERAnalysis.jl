# LASERAnalysis.jl Documentation

`LASERAnalysis.jl` is a package for analyzing various laser related
measurements. Examples include laser LIV (power-current-voltage) measurements
and optical spectra.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/LASERAnalysis.jl
```

## Features

* Basic LIV measurement analysis
* Spectral peak finding
* Spectral shift determination (for spectrum alignment)
* Spectral mode clustering (identify peaks in a series of shifting spectra and cluster them into spectral modes)

## Related Packages

* [ModeAnalysis.jl](https://gitlab.com/pawelstrzebonski/ModeAnalysis.jl) for analyzing transverse modes using beam images
* [ModeAnalysisML.jl](https://gitlab.com/pawelstrzebonski/ModeAnalysisML.jl) for analyzing transverse modes using machine learning analysis of beam images

