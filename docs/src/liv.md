# liv.jl

Functions for extracting the basic electrical/power characteristics
of a diode laser device given its power-current-voltage measurements.

```@autodocs
Modules = [LASERAnalysis]
Pages   = ["liv.jl"]
```
