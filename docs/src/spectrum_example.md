# Spectrum Analysis

The optical spectrum of a laser can reveal vital information regarding
the number (and perhaps even the nature) of the lasing modes. First,
we load an example laser spectrum from a VCSEL:

```@example spectrum
import Plots: plot, scatter!
import Plots: savefig # hide

# We import the spectrum as (wavelength, power)
include("spectrum_data.jl") # hide

plot(wavelength, power,
	xlabel="Wavelength [nm]",
	ylabel="Power [dBm]",
	legend=false,
	ylim=(-90, maximum(power)+10),
)
savefig("spectrum.png"); nothing # hide
```

![VCSEL spectrum](spectrum.png)

We can see that this laser has multiple modes currently lasing by the
multiple peaks in the spectrum. We can determine the wavelengths and powers
of these modes by finding the locations of these peaks using the
`findpeaks` function. While this function only requires the wavelength
and power information for a spectrum, it takes a pair of arguments
`mindist` and `zthresh` that you may need to set to get proper operation.
`mindist` determines the minimal distance
between peaks (if this is too low, then some peaks may be skipped over,
if too high then we may have spurious peaks found) and `zthresh`
determines the lower cut-off for point power (if this is too low then
peaks may be found in the noise-floor, if too high than some mode peaks
may be skipped). The "good" values for these settings are dependent
on the laser system (such as the typical mode spectral splitting)
and measurement setup (relation between modal peaks and the noise floor).

With some trial and error we can adjust the function arguments to find
the following mode peaks in the spectrum:

```@example spectrum
import LASERAnalysis

# Wavelength spacing
dl=wavelength[2]-wavelength[1]

# Determine the minimal peak spacing (0.2 [nm]) in terms of sample points
mindist=Int64(div(0.2, dl))

# We locate the peaks
peak_idx=LASERAnalysis.findpeaks(
	power,
	distance=mindist, height=-70
)
peak_wavelengths, peak_powers=wavelength[peak_idx], power[peak_idx]

plot(wavelength, power,
	xlabel="Wavelength [nm]",
	ylabel="Power [dBm]",
	legend=false,
	ylim=(-90, maximum(power)+10),
)
scatter!(peak_wavelengths, peak_powers)
savefig("peaks.png"); nothing # hide
```

![VCSEL spectrum with peaks marked](peaks.png)
