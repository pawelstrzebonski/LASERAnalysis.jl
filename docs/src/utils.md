# utils.jl

A collection of misc utility functions for the package.

```@autodocs
Modules = [LASERAnalysis]
Pages   = ["utils.jl"]
```
