using Documenter
import LASERAnalysis

makedocs(
    sitename = "LASERAnalysis.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "LASERAnalysis.jl"),
    pages = [
        "Home" => "index.md",
        "Examples" => [
            "LIV Analysis" => "liv_example.md",
            "Optical Spectrum Analysis" => "spectrum_example.md",
        ],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "filtering.jl" => "filtering.md",
            "liv.jl" => "liv.md",
            "spectrum.jl" => "spectrum.md",
            "utils.jl" => "utils.md",
        ],
        "test/" => [
            "filtering.jl" => "filtering_test.md",
            "liv.jl" => "liv_test.md",
            "spectrum.jl" => "spectrum_test.md",
            "utils.jl" => "utils_test.md",
        ],
    ],
)
