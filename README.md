# LASERAnalysis

## About

`LASERAnalysis.jl` is a package for analyzing various laser related
measurements. Examples include laser LIV (power-current-voltage) measurements
and optical spectra.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/LASERAnalysis.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for LASERAnalysis.jl](https://pawelstrzebonski.gitlab.io/LASERAnalysis.jl/).
