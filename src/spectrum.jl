import Clustering, FindPeaks, FFTW

#TODO: Taken from FindPeaks source. Make documenting this nicer somehow.
"""
    findpeaks(x; height=nothing, threshold=nothing, prominence=nothing, distance=nothing, width=nothing)


Find the peaks in `x` and return their indices in `x`.

# Arguments
- `x::AbstractVector`: array within which we look for peaks.
- `height=nothing`: minimal height (value) for a peak.
- `threshold=nothing`: minimal height difference on both sides for a peak.
- `prominence=nothing`: minimal topological prominence for a peak.
- `distance=nothing`: minimal spacing between peaks.
- `width=nothing`: minimal width of peaks.
"""
findpeaks = FindPeaks.findpeaks

"""
    findspectrumshift(p1, p2)

Find the spectral shift (in number of samples) to obtain spectral power
`p2` from `p1` using phase correlation.
"""
function findspectrumshift(p1::AbstractVector, p2::AbstractVector)
    P1, P2 = FFTW.fft(p1), FFTW.fft(p2)
    R = (P1 .* conj.(P2))
    R ./= abs.(R)
    r = FFTW.ifft(R)
    r = abs.(r)
    shifts = FFTW.fftfreq(length(p1)) .* length(p1)
    shifts[findmax(r)[2]]
end

"""
    findspectralmodes(wavelengths, powers; height=nothing, distance=nothing, width=nothing, prominence=nothing, extramodes=5, compensate_shift=false, cluster_method=:dbscan, radius_wavelength=0.05, radius_scans=3)->(peak_index, mode_index)

Find the modal peaks in a set of spectra and categorize them into spectral
modes.

# Arguments
- `wavelengths::AbstractVector`: an array of wavelength scan arrays.
- `powers::AbstractVector`: an array of power scan arrays.
- `height=nothing`: minimum height for peaks.
- `distance=nothing`: minimal distance in wavelength units between peaks.
- `extramodes::Number=5`: consider this many modes in excess of the maximal number of peaks found in any single scan (for k-means).
- `compensate_shift::Bool=false`: whether to try to compensate for wavelength shift.
- `cluster_method=:dbscan`: the clustering method to use (either `:dbscan` or `:kmeans`).
- `radius_wavelength=0.05`: radius on wavelength axis used for DBSCAN (in wavelength units).
- `radius_scans=3`: radius on the scan axis used for DBSCAN (in number of scans).

This function will first align the spectra to each-other if
`compensate_shift` is set to `true` (implicit assumption
is that the spectra are ordered so that adjacent spectra are most similar
to each-other) using the `findspectrumshift` function.
Then, peaks are found on the (shifted) spectra using the `findpeaks`
function.

Once the peaks are found, then we use cluster analysis to assign them to
different spectral modes.

k-means clustering in wavelength space will be used if
`cluster_method=:kmeans`.
As it is unknown a priori how many modes we
should have, we will try a couple different numbers of modes, ranging
from the maximal number of peaks we found in any spectrum, to that number
plus `extramodes`. The best result is found using silhouettes method.

DBSCAN clustering in wavelength and scan-number space will be used if
`cluster_method=:dbscan`. This method will automatically determine the
number of clusters/modes. In order to properly cluster the peaks, we must
specify appropriate radii (related to the distance between peaks for them
to be considered part of the same mode). These are set separately on the
wavelength axis and scan-number axis using `radius_wavelength` and
`radius_scans`.

Once the peaks have been clustered, we calculate and return the indices
of the peaks in each spectrum (as an array of arrays), as well as which
mode each peak belongs to (as an array of arrays).
"""
function findspectralmodes(
    wavelengths0::AbstractVector,
    powers0::AbstractVector;
    height = nothing,
    distance = nothing,
    width = nothing,
    prominence = nothing,
    extramodes::Integer = 5,
    compensate_shift::Bool = false,
    cluster_method = :dbscan,
    radius_wavelength = 0.05,
    radius_scans = 3,
)
    @assert length(wavelengths0) == length(powers0)
    # Deep-copy to avoid overwriting the input arrays
    wavelengths = deepcopy(wavelengths0)
    powers = deepcopy(powers0)
    Nscans = length(wavelengths)

    # Figure out the spectral shift between subsequent spectra
    shifts = zeros(Nscans)
    if compensate_shift
        shifts = [findspectrumshift(powers[i-1], powers[i]) for i = 2:length(powers)]
        shifts = [0; cumsum(shifts)]
    end

    # Shift the wavelengths to compensate for spectral shift
    dw = wavelengths[1][2] - wavelengths[1][1]
    wavelengths = [wavelengths[i] .+ dw * shifts[i] for i = 1:length(wavelengths)]

    # Find the peaks of all of the spectra
    peak_index = [
        findpeaks(
            powers[i],
            height = height,
            distance = distance,
            width = width,
            prominence = prominence,
        ) for i = 1:Nscans
    ]
    peak_wavelengths = [wavelengths[i][peak_index[i]] for i = 1:Nscans]
    peak_powers = [powers[i][peak_index[i]] for i = 1:Nscans]

    # Use cluster analysis to segregate the peaks into modes
    all_peak_wavelengths = reduce(vcat, peak_wavelengths)[:, :]'
    assignments = zeros(Int64, length(all_peak_wavelengths[:]))

    if cluster_method == :kmeans
        # Try a variety of mode numbers (lower limit at maximal number of modes found in a scan)
        max_peaks_found = maximum(length.(peak_wavelengths))
        ks = max_peaks_found .+ (0:extramodes)
        #TODO: maxiter=100 is arbitrary, do we even need/want to specify?
        R = [Clustering.kmeans(all_peak_wavelengths, k, maxiter = 100) for k in ks]

        # Determine which number of modes works best by finding the one that has the average silhouette closest to 1
        # Create matrix of distances between all the peaks
        D = [abs(a - b) for a in all_peak_wavelengths[:], b in all_peak_wavelengths[:]]
        silhouettescores = [mean(Clustering.silhouettes(R, D)) for R in R]
        bestres = findmax(silhouettescores)[2]
        # Now go with the best results
        Nmodes = ks[bestres]
        assignments = R[bestres].assignments
    elseif cluster_method == :dbscan
        #TODO: boundary points will be ignored, uncategorized
        #TODO: use 3D wavelength-current-power space?
        all_peak_scan_number =
            reduce(vcat, fill.(1:Nscans, length.(peak_wavelengths)))[:, :]'
        # Normalize the coordinates according to the radius parameters so that the DBSCAN radius is 1
        X = [
            all_peak_wavelengths ./ radius_wavelength
            all_peak_scan_number ./ radius_scans
        ]
        R = Clustering.dbscan(X, 1)
        assignments = R.assignments
    else
        @error "Bad `cluster_method` value."
    end

    # Convert the list of assignments into a list of per-spectrum assignments
    mode_index = empty([[1]])
    idxs = [0; cumsum(length.(peak_index))]
    for i = 1:Nscans
        append!(mode_index, [assignments[(1+idxs[i]):idxs[i+1]]])
    end

    peak_index, mode_index
end

"""
    spectralpowerinbucket(spectrum::AbstractVector)->(widths, powers, centers)

Calculate the maximal spectral power within a bucket given a `spectrum`.
Returns a set of vectors for the bucket `widths`, the maximal `powers`
contained within any given width, and the `centers` indices (or values
between indices) of the buckets.
"""
function spectralpowerinbucket(spectrum::AbstractVector)
    # Calculate integral image for simpler bandwidth calculation
    integratepower = cumsum(spectrum)
    # Length of spectrum
    N = length(spectrum)
    # Widths of bandwidth
    widths = 1:(N-1)
    results = [
        begin
            # Calculate all power in buckets for a given width
            pibs = integratepower[width.+(1:(N-width))] .- integratepower[1:(N-width)]
            # And return the maximal power in bucket
            findmax(pibs)
        end for width in widths
    ]
    # Unpack findmax results into values and indices
    toppowers, centers = getindex.(results, 1), getindex.(results, 2)
    # Translate index in the difference arrays into indices in the spectrum
    centers = centers .+ widths ./ 2
    widths, toppowers, centers
end
