"""
    mean(x)

Arithmetic mean of `x`.
"""
mean(x::AbstractArray) = sum(x) / length(x[:])

"""
    leastsquaresfit(x, y)->(a, b)

Determine the least-squares fit coefficients `a` and `b` such that
`y=a+b*x`.
"""
function leastsquaresfit(x::AbstractVector, y::AbstractVector)
    @assert length(x) == length(y)
    X = [x ones(length(x))]
    beta = inv(X' * X) * X' * y
    beta[2], beta[1]
end
