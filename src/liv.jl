"""
    livthreshold(l, i, v)->(i_th, v_th, idx)

Find the threshold current and voltage from LIV measurements.

The threshold is calculated to be the point of with the highest second
derivative of `l` with respect to `i`. The threshold current and voltage
are returned, along with the threshold's index in the input arrays.
"""
function livthreshold(l::AbstractVector, i::AbstractVector, v::AbstractVector)
    dldi = diff(l) ./ diff(i)
    d2ldi2 = diff(dldi) ./ (i[3:end] .- i[1:end-2]) ./ 2
    th_ind = findmax(d2ldi2)[2] + 1
    i[th_ind], v[th_ind], th_ind
end

"""
    livstats(l, i, v)->(i_th, v_th, r_s, v_d)

Calculate the threshold current `i_th`, threshold voltage `v_th`, series
resistance `r_s`, and diode voltage `v_d` from LIV measurements.
"""
function livstats(l::AbstractVector, i::AbstractVector, v::AbstractVector)
    # First find the threshold
    i_th, v_th, idx = livthreshold(l, i, v)
    # Determine the linear fit for the above-threshold I-V curve
    a, b = leastsquaresfit(i[idx:end], v[idx:end])
    # The constant term is taken to be the diode voltage
    v_d = a
    # The series resistance is taken to be the slope
    r_s = b
    i_th, v_th, r_s, v_d
end
