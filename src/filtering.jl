#TODO: More elegant/efficient convolution?

"""
    gaussianmatrix(sigma, radius)

Generate the (1D) Gaussian matrix of a given radius and decay factor.
"""
gaussianmatrix(sigma::Number, radius::Integer) = (
    G = [exp(-x^2 / (2 * sigma^2)) / sqrt(2 * pi * sigma^2) for x = -radius:radius]; G ./ sum(G)
)

"""
    gaussianfilter(x, sigma, radius)

Use Gaussian filtering (Gaussian parameter `sigma` and window radius
`radius`) to smooth `x`.
"""
function gaussianfilter(x::AbstractVector, sigma::Number, radius::Integer)
    xpadded = [fill(x[1], radius); x; fill(x[end], radius)]
    gfilt = gaussianmatrix(sigma, radius)
    xfilt = [sum(gfilt .* xpadded[i:(i+2radius)]) for i = 1:length(x)]
    xfilt
end
