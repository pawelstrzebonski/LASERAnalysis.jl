@testset "spectrum.jl" begin
    dw = 0.25
    w = 800:dw:900
    p0 = @. exp(-(w - 840)^2) + exp(-(w - 850)^2)
    shift = 42
    pshifted = circshift(p0, shift)
    wshifted = circshift(w, shift)
    pshifted2 = circshift(p0, -shift)
    wshifted2 = circshift(w, -shift)

    @test (LASERAnalysis.findspectrumshift(p0, pshifted) == -shift)
    @test (LASERAnalysis.findspectrumshift(p0, pshifted2) == shift)

    @test (LASERAnalysis.findpeaks(p0, height = 0.5) == indexin([840, 850], w))

    # Simplest case of all the same spectrum should easily group the modes
    @test (
        LASERAnalysis.findspectralmodes([w, w, w], [p0, p0, p0], height = 0.5) ==
        (fill(indexin([840, 850], w), 3), fill([1, 2], 3))
    )

    # Test trivial case with radius so that all modes fall into one mode
    @test (
        LASERAnalysis.findspectralmodes(
            [w, w, w],
            [p0, p0, p0],
            height = 0.5,
            radius_wavelength = 10,
        ) == (fill(indexin([840, 850], w), 3), fill([1, 1], 3))
    )

    # Compensating allows for proper mode identification
    @test (
        LASERAnalysis.findspectralmodes(
            [w, w, w],
            [pshifted2, p0, pshifted],
            height = 0.5,
            compensate_shift = true,
        ) == ((x -> indexin([840, 850], x)).([wshifted2, w, wshifted]), fill([1, 2], 3))
    )

    # Using k-means with same spectral will group the modes fine, but actual mode index may vary
    peak_idx, mode_idx = LASERAnalysis.findspectralmodes(
        [w, w, w],
        [p0, p0, p0],
        height = 0.5,
        cluster_method = :kmeans,
        extramodes = 2,
    )
    @test (peak_idx == fill(indexin([840, 850], w), 3))
    @test (mode_idx == fill([1, 2], 3) || mode_idx == fill([2, 1], 3))

    # The peaks are not realigned and identified "properly"
    peak_idx, mode_idx = LASERAnalysis.findspectralmodes(
        [w, w, w],
        [pshifted2, p0, pshifted],
        height = 0.5,
        cluster_method = :kmeans,
        extramodes = 2,
    )
    @test (peak_idx == (x -> indexin([840, 850], x)).([wshifted2, w, wshifted]))
    @test (mode_idx[1][2] == mode_idx[2][1] && mode_idx[2][2] == mode_idx[3][1])
    @test (mode_idx[1][1] != mode_idx[1][2] != mode_idx[2][2] != mode_idx[3][2])

    # After compensating for shift we can properly cluster the peaks
    peak_idx, mode_idx = LASERAnalysis.findspectralmodes(
        [w, w, w],
        [pshifted2, p0, pshifted],
        height = 0.5,
        cluster_method = :kmeans,
        extramodes = 2,
        compensate_shift = true,
    )
    @test (peak_idx == (x -> indexin([840, 850], x)).([wshifted2, w, wshifted]))
    @test (mode_idx == fill([1, 2], 3) || mode_idx == fill([2, 1], 3))

    res = 0
    @test (res = LASERAnalysis.spectralpowerinbucket(p0); true)
    # Widths are increasing by 1
    @test all(isone.(diff(res[1])))
    # Powers should be non-decreasing
    @test all(diff(res[2]) .>= 0)
    # Centers should be within bounds of array
    @test all(1 .<= res[3] .<= length(p0))
end
