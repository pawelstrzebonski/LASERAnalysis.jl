@testset "utils.jl" begin
    @test LASERAnalysis.mean(1:10) == 5.5
    @test LASERAnalysis.mean(ones(10)) == 1

    x = 1:10
    y = @. 3.5 - 1.25 * x
    @test approxeq(LASERAnalysis.leastsquaresfit(x, y), (3.5, -1.25))
end
