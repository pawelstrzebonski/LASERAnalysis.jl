@testset "liv.jl" begin
    # Define a function to create fake LIV data
    function livfun(i, i_th, v_d, r_s, eta)
        v = @. v_d + i * r_s
        l = @. (i - i_th > 0) * (i - i_th) * v_d * eta
        l, v
    end

    i = collect(0:1:10)
    i_th = 3
    v_d = 1
    r_s = 2
    eta = 0.5
    l, v = livfun(i, i_th, v_d, r_s, eta)

    @test LASERAnalysis.livthreshold(l, i, v) == (i_th, 7, 4)
    @test LASERAnalysis.livthreshold(l ./ 2, i, v) == (i_th, 7, 4)
    @test LASERAnalysis.livthreshold(l, i ./ 2, v) == (i_th / 2, 7, 4)
    @test LASERAnalysis.livthreshold(l, i, v ./ 2) == (i_th, 7 / 2, 4)

    @test approxeq(LASERAnalysis.livstats(l, i, v), (i_th, 7, r_s, v_d))
    @test approxeq(LASERAnalysis.livstats(l, i, fill(v_d, length(i))), (i_th, 1, 0, v_d))
    @test approxeq(LASERAnalysis.livstats(l ./ 2, i, v), (i_th, 7, r_s, v_d))
    @test approxeq(LASERAnalysis.livstats(l, i ./ 2, v), (i_th / 2, 7, 2 * r_s, v_d))
    @test approxeq(LASERAnalysis.livstats(l, i, v ./ 2), (i_th, 7 / 2, r_s / 2, v_d / 2))
end
