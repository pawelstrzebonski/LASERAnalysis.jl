@testset "filtering.jl" begin
    @test length(LASERAnalysis.gaussianmatrix(1, 1)) == 3
    @test length(LASERAnalysis.gaussianmatrix(1, 3)) == 7
    @test length(LASERAnalysis.gaussianmatrix(3, 3)) == 7
    @test sum(LASERAnalysis.gaussianmatrix(1, 1)) == 1
    @test sum(LASERAnalysis.gaussianmatrix(1, 3)) == 1
    @test approxeq(sum(LASERAnalysis.gaussianmatrix(3, 3)), 1)
    @test LASERAnalysis.gaussianmatrix(3, 3) == LASERAnalysis.gaussianmatrix(-3, 3)
    @test LASERAnalysis.gaussianmatrix(3, 3) == reverse(LASERAnalysis.gaussianmatrix(3, 3))
    @test approxeq(LASERAnalysis.gaussianmatrix(1, 1), [0.2740686, 0.451862, 0.2740686])

    x = (0:10) .^ 2
    @test length(LASERAnalysis.gaussianfilter(x, 1, 1)) == length(x)
    @test LASERAnalysis.gaussianfilter(ones(10), 1, 1) == ones(10)
end
